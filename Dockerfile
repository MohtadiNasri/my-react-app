FROM node:12.22-buster

WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY . /app
EXPOSE 9000
CMD ["npm","run", "start"]

